<?php

namespace App\Http\Controllers;

use App\Imports\MailImport;
use App\Imports\TimekeepingBeforeImport;
use App\Imports\TimekeepingBeforeImportCH;
use App\Imports\TimekeepingBeforeImportVP;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        if ($request->files) {
            $file = $request->file('file');
            Excel::import(new MailImport, $file);

            return 1;
        }

        return 0;
    }

    public function handleTimeKeepingVP(Request $request)
    {
        if ($request->files) {
            $file = $request->file('file');
            Excel::import(new TimekeepingBeforeImportVP, $file);

            return response()->download(storage_path('app/ChamCongVP.xlsx'));
        }

        return 0;
    }

    public function handleTimeKeepingCH(Request $request)
    {
        if ($request->files) {
            $file = $request->file('file');
            Excel::import(new TimekeepingBeforeImportCH, $file);

            return response()->download(storage_path('app/ChamCongCH.xlsx'));
        }

        return 0;
    }
}
