<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $row;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($row)
    {
        $this->row = $row;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $month = now()->month - 1;
        return $this->from('anhnn@thinkview.vn')->subject("THINKVN - PHIẾU LƯƠNG THÁNG $month")->view('mail', ['row' => $this->row]);
    }
}
