<?php

namespace App\Imports;

use App\Mail\SendMail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class MailImport implements ToCollection, WithCalculatedFormulas, WithHeadingRow
{
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $email = $row['email'];
            if (!empty($email)) {
                Mail::to($email)->queue(new SendMail($row));
            }
        }
    }

    public function headingRow()
    {
        return 2;
    }
}
