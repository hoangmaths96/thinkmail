<?php

namespace App\Imports;

use App\Exports\BangChamCong;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;


class TimekeepingBeforeImportVP implements ToCollection, WithCalculatedFormulas, WithHeadingRow
{
    public function collection(Collection $collection)
    {
        $numberOfDays = 31;
        $firstResult = ['Họ và tên'];
        for ($i = 1; $i <= $numberOfDays; $i++) {
            $firstResult[] = $i;
        }
        $firstResult = array_merge($firstResult, ['Tổng công', 'Làm thêm', 'Đi muộn <10ph', 'Đi muộn <30ph', 'Đi muộn <60ph', 'Đi muộn >60ph', 'Quên chấm công']);
        $result = [$firstResult];
        foreach ($collection->chunk(2) as $chunk) {
            $name = trim($chunk->first()['name']);
            $willAddResult = [$name];
            $tongCong = 0;
            $lamThem = 0;
            $muon10 = 0;
            $muon30 = 0;
            $muon60Kem = 0;
            $muon60Hon = 0;
            $quen = 0;
            $checkIn = [];
            $checkOut = [];
            for ($i = 1; $i <= $numberOfDays; $i++) {
                if (isset($chunk->first()[$i]) && !empty($chunk->first()[$i])) $checkIn[$i] = Carbon::parse($chunk->first()[$i])->toTimeString();
                else $checkIn[$i] = null;
            }
            for ($i = 1; $i <= $numberOfDays; $i++) {
                if (isset($chunk->skip(1)->first()[$i]) && !empty($chunk->skip(1)->first()[$i])) $checkOut[$i] = Carbon::parse($chunk->skip(1)->first()[$i])->toTimeString();
                else $checkOut[$i] = null;
            }
            for ($i = 1; $i <= $numberOfDays; $i++) {
                if (is_null($checkIn[$i]) && is_null($checkOut[$i])) {
                    $willAddResult[] = 0;
                } else {
                    $willAddResult[] = 1;
                    $tongCong++;
                    if (is_null($checkIn[$i]) || is_null($checkOut[$i])) {
                        $quen++;
                    }
                    if (!is_null($checkIn[$i])) {
                        if ($checkIn[$i] > Carbon::parse('8:30')->toTimeString()) {
                            if ($checkIn[$i] <= Carbon::parse('8:40')->toTimeString()) {
                                $muon10++;
                            } elseif ($checkIn[$i] <= Carbon::parse('9:00')->toTimeString()) {
                                $muon30++;
                            } elseif ($checkIn[$i] <= Carbon::parse('9:30')->toTimeString()) {
                                $muon60Kem++;
                            } else {
                                $muon60Hon++;
                            }
                        }
                    }
                }
            }
            $willAddResult[] = $tongCong;
            $willAddResult[] = $lamThem;
            $willAddResult[] = $muon10;
            $willAddResult[] = $muon30;
            $willAddResult[] = $muon60Kem;
            $willAddResult[] = $muon60Hon;
            $willAddResult[] = $quen;
            $result[] = $willAddResult;
        }

        Excel::store(new BangChamCong($result), 'ChamCongVP.xlsx');
    }

    public function headingRow()
    {
        return 1;
    }
}
