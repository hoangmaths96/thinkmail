@php
    function product_price($priceFloat) {
    $symbol = '';
    $symbol_thousand = ',';
    $decimal_place = 0;
    $price = number_format($priceFloat, $decimal_place, '', $symbol_thousand);
    return $price.$symbol;
    }
    function getSoLuong($option) {
        return $option->serials ? count($option->serials) : 0;
    }
    function getMoney($option) {
        return getSoLuong($option) * $option->price;
    }
    function address($banhang) {
        $sonha = $banhang->khachhang->so_nha ? $banhang->khachhang->so_nha.', ' : '';
        $phuong_xa = $banhang->khachhang->phuong_xa ? $banhang->khachhang->phuong_xa.', ' : '';
        $quan_huyen = $banhang->khachhang->quan_huyen ? $banhang->khachhang->quan_huyen.', ' : '';
        $tinh_thanhpho = $banhang->khachhang->tinh_thanhpho ? $banhang->khachhang->tinh_thanhpho : '';
        return $sonha.$phuong_xa.$quan_huyen.$tinh_thanhpho;
    }
@endphp
<div style="font-style: italic">Gửi <span style="color: red">{{$row['hovaten']}},</span></div>
<br>
<div style="font-weight: bold">Công ty TNHH Công nghệ Think Việt Nam gửi <span
        style="color: red">{{$row['hovaten']}}</span> PHIẾU LƯƠNG tháng 12</div>
<br>
<div style="font-weight: bold; font-size: 18px">PHIẾU LƯƠNG THÁNG 12 - LẦN I</div>
<br>
<table style="border-collapse: collapse;width: 100%;">
    <tr>
        @if(!is_null($row['hovaten']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Họ và tên
            </th>
        @endif
        @if(!is_null($row['chucdanh']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Chức danh
            </th>
        @endif
        @if(!is_null($row['luongchinh']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Lương chính
            </th>
        @endif
        @if(!is_null($row['ngaycong']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
padding-bottom: 12px;
text-align: left;
background-color: cornflowerblue;
color: white;">Ngày công
            </th>
        @endif
        @if(!is_null($row['giocong']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Giờ công
            </th>
        @endif
        @if(!is_null($row['tienluong']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Tiền lương
            </th>
        @endif
        @if(!is_null($row['nclng']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Ngày công làm ngoài giờ
            </th>
        @endif
        @if(!is_null($row['llng']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Lương làm ngoài giờ
            </th>
        @endif
        @if(isset($row['pc']) && !empty($row['pc']) && !is_null($row['pc']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Phụ cấp
            </th>
        @endif
        @if(!is_null($row['thuong']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Thưởng
            </th>
        @endif
        @if(!is_null($row['tongluong']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Tổng lương
            </th>
        @endif
        @if(!is_null($row['phat']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Phạt vi phạm
            </th>
        @endif
        @if(!is_null($row['baohiem']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Đóng bảo hiểm
            </th>
        @endif
        @if(!is_null($row['ung']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Tạm ứng
            </th>
        @endif
        @if(!is_null($row['tl']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Thực lĩnh
            </th>
        @endif
        @if(!is_null($row['ghichu']))
            <th style="border: 1px solid #ddd;padding: 8px;padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: cornflowerblue;
        color: white;">Ghi chú
            </th>
        @endif
    </tr>
    <tr>
        @if(!is_null($row['hovaten']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['hovaten']}}</td>
        @endif
        @if(!is_null($row['chucdanh']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['chucdanh']}}</td>
        @endif
        @if(!is_null($row['luongchinh']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['luongchinh'])}}</td>
        @endif
        @if(!is_null($row['ngaycong']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['ngaycong']}}</td>
        @endif
        @if(!is_null($row['giocong']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['giocong']}}</td>
        @endif
        @if(!is_null($row['tienluong']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int) $row['tienluong'])}}</td>
        @endif
        @if(!is_null($row['nclng']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['nclng']}}</td>
        @endif
        @if(!is_null($row['llng']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['llng'])}}</td>
        @endif
        @if(isset($row['pc']) && !empty($row['pc']) && !is_null($row['pc']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['pc '])}}</td>
        @endif
        @if(!is_null($row['thuong']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['thuong'])}}</td>
        @endif
        @if(!is_null($row['tongluong']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int) $row['tongluong'])}}</td>
        @endif
        @if(!is_null($row['phat']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int) $row['phat'])}}</td>
        @endif
        @if(!is_null($row['baohiem']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['baohiem'])}}</td>
        @endif
        @if(!is_null($row['ung']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int)$row['ung'])}}</td>
        @endif
        @if(!is_null($row['tl']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{product_price((int) $row['tl'])}}</td>
        @endif
        @if(!is_null($row['ghichu']))
            <td style="border: 1px solid #ddd;padding: 8px;">{{$row['ghichu']}}</td>
        @endif
    </tr>
</table>
<br>
<div style="font-style: italic">Mọi thắc mắc về phiếu lương, nhân sự vui lòng phản hồi tới Ms. Ánh để được giải đáp.</div>
<br>
<div style="font-style: italic">Trân thành cảm ơn!</div>
